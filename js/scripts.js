/**
 * Created by Acer on 05.01.2019.
 */
$(document).ready(function(){
    $('.lists--content').on('mousedown', function(e){
        var elem_drag = $(this);
        $(this).addClass('drag-block');

        var shiftX = e.pageX - elem_drag.children('.lists--list_wrapper').offset().left;
        var shiftY = e.pageY - elem_drag.children('.lists--list_wrapper').offset().top;

        function moveAt(e) {
            elem_drag.css({
                'left':e.pageX - shiftX,
                'top':e.pageY - shiftY
            });
        }

        elem_drag.mousemove(function(e){
            moveAt(e);
        });

        $('.lists').on('mouseup', function(){
            elem_drag.off('mousemove');
        });
    });

    $('.lists').on('mouseup', function(){
        $('drag-block').off();
    });
});